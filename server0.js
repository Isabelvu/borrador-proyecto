const express = require('express');
const app = express();

const port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto" + port);

app.get('/apitechu/v1/hello',
 function(req, res)   {
  console.log("GET /apitechu/v1/hello");
  res.send({"msg" : "Hola desde aqui cambiado a json"});
   }
)


// recupero el array de usuarios
app.get('/apitechu/v1/users',
 function(req, res)   {
//  res.sendFile('usuarios.json',{root:__dirname});
var users = require('./usuarios.json');
res.send(users);
   }
)

// mando un post a users para cerar un usuario
app.post('/apitechu/v1/users',
 function(req, res)   {
  console.log("POST /apitechu/v1/users");
// saco por consola las cabeceras  console.log(req.headers);
console.log(req.headers.first_name);
console.log(req.headers.last_name);
console.log(req.headers.email);

var newUser = {
"first_name": req.headers.first_name,
"last_name": req.headers.last_name,
"email": req.headers.email
}

var users = require('./usuarios.json');
users.push(newUser);
console.log("Usuario añadido con exitio");

const fs = require('fs');
var jsonUserData = JSON.stringify(users);
// operacion de escritura usando fs
fs.writeFile("./usuarios.json", jsonUserData, "utf8",
function (err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Usuario persistido");
      }
    }
)
}
)
