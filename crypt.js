const bcrypt = require("bcrypt");

function hash(data){
console.log("Encriptando los datos")
return bcrypt.hashSync(data, 10);
}
 console.log("Checking password2");
function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
// console.log("pass iguales");
 return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
 }

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
