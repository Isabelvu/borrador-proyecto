require("dotenv").config();

const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

app.use(express.json());
app.use(enableCORS);

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const CuentasController = require('./controllers/CuentasController');




app.listen(port);
console.log("API escuchando en el puerto cambio2 " + port);

app.get('/apitechu/v1/hello',
function(req, res)  {
   console.log("GET /apitechu/v1/hello");
  res.send({"msg" :"Hola desde API TechU"});
 }
)

// REGISTRO LAS RUTAS
app.get('/apitechu/v1/users',userController.getUsersV1);
// registro la ruta de v2
app.get('/apitechu/v2/users',userController.getUsersV2);
app.get('/apitechu/v2/users/:id',userController.getUsersByIdV2);
app.get('/apitechu/v2/cuentas/:idUsuario',CuentasController.getCuentasById);


app.post('/apitechu/v2/users',userController.createUsersV2);
app.post('/apitechu/v1/users',userController.createUsersV1);
app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);
// post a login
app.post('/apitechu/v1/login',authController.loginUsersV1);
app.post('/apitechu/v2/login',authController.loginUsersV2);
// post a logout
app.post('/apitechu/v1/logout/:id',authController.logoutUsersV1);
app.post('/apitechu/v2/logout/:id',authController.logoutUsersV2);



app.post("/apitechu/v1/monstruo/:p1/:p2",
 function (req, res) {
   console.log("Parámetros");
   console.log(req.params);

   console.log("Query String");
   console.log(req.query);

   console.log("Headers");
   console.log(req.headers);

   console.log("Body");
   console.log(req.body);

 }
)







//app.post('/apitechu/v1/users',userController.deleteUsersV1);
// funcion de creacion del usuario *****   POST ***
// el dia 22 donde pone headers pongo bodypara refatorizar
// app.post('/apitechu/v1/users',
//  function(req, res) {
//    console.log("POST /apitechu/v1/users");
//    console.log(req.body.first_name);
//    console.log(req.body.last_name);
//    console.log(req.body.email);
//
//    var newUser = {
//      "first_name": req.body.first_name,
//      "last_name":req.body.last_name,
//      "email":req.body.email
//    }
//  //traemos fichero de usuarios a continuacion
//
//    var users = require('./usuarios.json');
//    users.push(newUser);
// //invocamos a la funcion nueva
//    writeUserDatatoFile(users);
//    console.log("Usuario añadido con exito");
//    res.send({"msg" : "Usuario añadido con exito"})
//
//    }
// )


//Creamos funcion nueva borrado. el splice muta la lista
//(req.params) Checks route params, ex: /user/:id

//app.delete("/apitechu/v1/users/:id",
//  function(req, res){
//    console.log("DELETE/apitechu/v1/users/:id");
//    console.log("Id es el " + req.params.id);
// //traemos el fichero de usuarios
//    var users = require('./usuarios.json');
//    var deleted = false;
//    var i;
//    for (i = 0; i < users.length; i++)  {
//      console.log("El indice vale" + i);
//      console.log("comparando " + users[i].id + " y " + req.params.id);
//      console.log("Me quedan los siguientes usuarios en el array" + users.length);
// //    length	Sets or returns the number of elements in an array
//       if (users[i].id == req.params.id) {
//       console.log("parametro de la URL " + req.params.id);
//   //    quiero borrar ese parametro
//           console.log("paso por splice y el indice vale " + i);
//           users.splice(i,1);
//           writeUserDatatoFile(users);
//           res.send({"msg" : "Usuario borrado con exito por postman"})
//           console.log("Usuario borrado");
//           deleted = true;
//           break; // se sale de aqui con el break
//    }
//  };
//  }
// )


//creo la nueva funcion nueva borrado con el for each. Modifico la ruta de postman
//(req.params) Checks route params, ex: /user/:id

//var miArray = [ 2, 4, 6, 8, 10 ];
//miArray.forEach( function(valor, indice, array) {
//    console.log("En el índice " + indice + " hay este valor: " + valor);
//});
// //
// app.delete("/apitechu/v1/users/For1/:id",
//  function(req, res){
//    console.log("DELETE/apitechu/v1/users/For1/:id");
//    console.log("Id es el " + req.params.id);
//    var users = require('./usuarios.json');
//    //var i;
//    users.forEach (function (user, i ,users)  {
//      console.log("Entros en for each");
//      console.log("El indice vale" + i);
//      console.log("Me quedan los siguientes usuarios en el array" + users.length);
// //    length	Sets or returns the number of elements in an array
//       if (i == req.params.id) {
//       console.log("parametro de la URL " + req.params.id);
//   //    quiero borrar ese parametro
//           console.log("paso por splice y el indice vale " + i);
//           users.splice(i,1);
//           writeUserDatatoFile(users);
//           console.log("Usuario borrado");
//         }
//  }
//  )
// }
// );
//
