//Creamos un nuevo controller para hacer el login y el logout
const io = require('../io');
const crypt = require("../crypt");
const requestJson = require("request-json");
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuiv10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// Comenzamos con el LOGIN - Hacemos un post y descargamos el fichero
//  Lo recorremos para comparar usuario y pass. Cuando coinciden ponemos el flag a true

function loginUsersV1(req, res) {
  console.log("POST /apitechu/v1/login");
  console.log(req.body.email);
  console.log(req.body.password);
  //traemos el fichero de usuarios
     var users = require('../usuarios.json');
       for (i = 0; i < users.length; i++)  {
       console.log("El valor del indice es" + i);
       if (users[i].email == req.body.email && users[i].Password == req.body.password)  {
           console.log("coinciden email y contraseña. El Email de usuario es  " + users[i].email)
        //   var userLogged = true;
           users[i].logged = true;
              io.writeUserDatatoFile(users);
              res.send({"msg" : "Usuario logado con exito ID " + users[i].id})
              break;
           } else {
                if  (users.length == users[i].id) {
                  console.log("Pasa por el else. Usuario logado?  " + users[i].logged)
                  res.send({"msg" : "Usuario o contraseña errónea"})
                }
            //
                };
            };
   };

   // practica 2 login



// FUNCION LOGOUT
function logoutUsersV1(req, res) {
  console.log("POST/apitechu/v1/logout/:id");
  console.log(req.body.email);
  console.log(req.body.password);
  //traemos el fichero de usuarios
     var users = require('../usuarios.json');
     for (i = 0; i < users.length; i++)  {
     if (users[i].logged == true && req.params.id == users[i].id)  {
               delete users[i].logged;
               io.writeUserDatatoFile(users);
               res.send({"msg" : "Logout realizado con exito"})
      } else {
             if  (users.length == users[i].id) {
               res.send({"msg" : "Error en el logout"})
           }
   };
}
}


function loginUsersV2(req, res) {
 console.log("POST /apitechu/v2/login");
//q=<query> - restrict results by the specified JSON query
//"q" example - return all documents with "active" field of true:
//https://api.mlab.com/api/1/databases/my-db/collections/my-coll?q={"active": true}&apiKey=myAPIKey
 var query = 'q={"email": "' + req.body.email + '"}';
  console.log("query es " + query);
 console.log(req.body);
//  creo el cliente http mediante la libreria requestJson
//const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuiv10ed/collections/";
 httpClient = requestJson.createClient(baseMLabURL);

 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     var isPasswordcorrect =
     crypt.checkPassword(req.body.Password, body[0].Password);
     console.log("Password correct is " + isPasswordcorrect);

     if (!isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}
function logoutUsersV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}
module.exports.loginUsersV1 = loginUsersV1 ;
module.exports.logoutUsersV1 = logoutUsersV1 ;
module.exports.loginUsersV2 = loginUsersV2 ;
module.exports.logoutUsersV2 = logoutUsersV2 ;
