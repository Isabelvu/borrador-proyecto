//aqui pongo los modulos que utilizamos como llamamos a writeUserDatatoFile tenemos que declararlo arriba
const io = require('../io');
const crypt = require("../crypt");
const requestJson = require("request-json");
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuiv10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

// **********************voy al get de users

function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");
//   res.sendFile('usuarios.json',{root:__dirname});
var result = {};
var users = require('../usuarios.json');
if (req.query.$count == "true") {
console.log("Count needed");
result.count = users.length;
}
result.users = req.query.$top ?
users.slice(0, req.query.$top) : users;
  res.send(result);
}

/// creamos una nueva getusers

function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");
// no vale con crear la libreria tambien hay que activarla.
//los clients se crean dentro de una URL Base createClient(URLBASE)

var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");
//obtener toda la lista de usuarios. solo poner user y el apikey
  httpClient.get("user?" + mLabAPIKey,
  function(err, resMLab, body) {
   var response = !err ? body :{
   "msg" : "Error obteniendo usuarios"
 }
  res.send(response);
})}


// creamos una nueva getUsersByIdV2

function getUsersByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
    console.log("la consulta es " + query);
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado con getUsersByIdV2");
  //obtener toda la lista de usuarios. solo poner user y el apikey ..'q={"id":3}'
    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuarios"
           }
         res.status(500);
         }else {
       if (body.length > 0) {
      var response = body[0];
      }else {
        var response = {
        "msg" : "Usuario nop encontrado"
      }
      res.status(404);
      }
    }
    res.send(response);
  }
)
}

// nueva
function createUsersV2(req, res) {
  console.log("POST /apitechu/v2/users");
  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.Password);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email,
    "Password": crypt.hash(req.body.Password),
  }

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado con createByIdV2");
//obtener toda la lista de usuarios. solo poner user y el apikey ..'q={"id":3}'
 //httpClient.post("user?" + mLabAPIKey,
 httpClient.post("user?" +  mLabAPIKey, newUser,
 function(err, resMLab, body) {
   console.log("Usuario creado en mLabAPIKey");
   res.status(201).send({"msg":"Usuario creadoo"});
}
)
}


function createUsersV1(req, res) {
  console.log("POST /apitechu/v1/users");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email
  }
  var users = require('../usuarios.json');
  users.push(newUser);
//invocamos a la funcion nueva
  io.writeUserDatatoFile2(users);
  console.log("Usuario añadido con exito");
  res.send({"msg" : "Usuario añadido con exito"})

  }


// delete
 function deleteUserV1(req, res){
   console.log("DELETE/apitechu/v1/users/:id");
   console.log("Id es el " + req.params.id);
//traemos el fichero de usuarios
   var users = require('../usuarios.json');
   var deleted = false;
   var i;
   for (i = 0; i < users.length; i++)  {
     console.log("El indice vale" + i);
     console.log("comparando " + users[i].id + " y " + req.params.id);
     console.log("Me quedan los siguientes usuarios en el array" + users.length);
//    length	Sets or returns the number of elements in an array
      if (users[i].id == req.params.id) {
      console.log("parametro de la URL " + req.params.id);
  //    quiero borrar ese parametro
          console.log("paso por splice y el indice vale " + i);
          users.splice(i,1);
          io.writeUserDatatoFile(users);
          res.send({"msg" : "Usuario borrado con exito por postman"})
          console.log("Usuario borrado");
          deleted = true;
          break; // se sale de aqui con el break
   }
 };
 }



 // despues del punto de exports digo que es lo que quiero exportr y se vea desde el exterior

module.exports.getUsersV1 = getUsersV1 ;
module.exports.createUsersV1 = createUsersV1 ;
module.exports.deleteUserV1 = deleteUserV1 ;

// EXPORTS de version 2
module.exports.getUsersV2 = getUsersV2 ;
module.exports.getUsersByIdV2 = getUsersByIdV2 ;

module.exports.createUsersV2 = createUsersV2 ;
